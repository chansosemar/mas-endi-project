import {combineReducers} from 'redux';
import loginReducer from '../Screen/Login/reducer';
import RegisterReducer from '../Screen/Register/reducer';
import profileReducer from '../Screen/Profile/reducer';
import GlobalReducer from './GlobalReducer';
import homeReducer from '../Screen/Home/Redux/reducer';
import {ReviewReducer} from '../Screen/Review/reducer';

export const allReducer = combineReducers({
  Global: GlobalReducer,
  Register: RegisterReducer,
  Login: loginReducer,
  Profile: profileReducer,
  Home: homeReducer,
  Review: ReviewReducer,
});
