import {createStore, applyMiddleware} from 'redux';
import {allReducer} from './allReducer';
import {persistReducer, persistStore} from 'redux-persist';
import logger from 'redux-logger';
import AsyncStorage from '@react-native-async-storage/async-storage';
import createSaga from 'redux-saga';
import rootSaga from './allSaga';

const persistConfig = {
  key: 'keyConfig',
  storage: AsyncStorage,
};

const sagaMiddleWare = createSaga();

const persistedReducer = persistReducer(persistConfig, allReducer);

export const Store = createStore(
  persistedReducer,
  applyMiddleware(logger, sagaMiddleWare),
);

export const Persistor = persistStore(Store);

sagaMiddleWare.run(rootSaga);
