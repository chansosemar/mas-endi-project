import {all} from 'redux-saga/effects';
import authSaga from '../Screen/Auth/authSaga';
import profileSaga from '../Screen/Profile/sagaProfile';
import {SagaHome} from '../Screen/Home/Redux/saga';
import updateProfileSaga from '../Screen/Profile/sagaUpdateProfile';

export default function* rootSaga() {
  yield all([SagaHome(), authSaga(), profileSaga(), updateProfileSaga()]);
}
