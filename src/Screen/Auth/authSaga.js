import {takeLatest, put} from 'redux-saga/effects';
import {registerApi, loginApi} from './authApi';
import {loginActionSuccess, loginActionFailed} from '../Login/action';
import {registerActionFailed, registerActionSuccess} from '../Register/action';
import {navigate} from '../../Function/nav';
import {saveToken} from '../utils/asynch';
import {ToastAndroid} from 'react-native';

function* loginAction(action) {
  try {
    console.log(action.payload);
    const res = yield loginApi(action.payload);
    console.log(res);
    if (res.status < 300) {
      ToastAndroid.showWithGravity(
        'Login Berhasil',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      yield navigate('MyBottomTab');
      yield saveToken(res.data.token);
      yield put(loginActionSuccess(res.data.result));
    } else {
      ToastAndroid.showWithGravity(
        'Login Gagal',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      yield put(loginActionFailed());
    }
  } catch (e) {
    ToastAndroid.showWithGravity(
      'Login Gagal',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
    yield put(loginActionFailed());
  }
}

function* registerAction(action) {
  try {
    console.log(action.payload, 'payload');
    const res = yield registerApi(action.payload);
    console.log(res, 'database');
    if (res && res.data && res.status < 300) {
      ToastAndroid.showWithGravity(
        'Registrasi Berhasil',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      // console.log(res.data);
      yield saveToken(res.data.token);
      yield put(registerActionSuccess(res.data.result));
      yield navigate('MyBottomTab');
    } else {
      ToastAndroid.showWithGravity(
        'Registrasi Gagal',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    }
  } catch (e) {
    console.log(e, 'gagal register');
    yield put(registerActionFailed());
  }
}

function* authSaga() {
  yield takeLatest('LOGIN', loginAction);
  yield takeLatest('REGISTER', registerAction);
}

export default authSaga;
