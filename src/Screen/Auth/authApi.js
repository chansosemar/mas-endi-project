import axios from 'axios';

export const registerApi = payload => {
  console.log(payload, 'dariaxios');
  return axios({
    method: 'POST',
    url: 'https://milantv-be.herokuapp.com/api/milantv/auth/register',
    data: payload,
    validateStatus: status => status < 500,
  });
};

export const loginApi = payload => {
  console.log(payload, 'dariaxios');
  return axios({
    method: 'POST',
    url: 'https://milantv-be.herokuapp.com/api/milantv/auth/login',
    data: payload,
    validateStatus: status => status < 500,
  });
};
