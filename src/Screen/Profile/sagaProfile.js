import {takeLatest, put} from 'redux-saga/effects';
import {getProfileApi} from './apiProfile';
import {getHeaders} from '../utils/asynch';

function* profileActionSaga() {
  try {
    const headers = yield getHeaders();
    const res = yield getProfileApi(headers);
    console.log(res, 'ini res');
    if (res && res.data) {
      yield put({type: 'PROFILE_SUCCESS', payload: res.data.data});
    } else {
      console.log('gagal ambil data profile');
      // yield put(loginActionFailed());
    }
  } catch (e) {
    console.log('gagal ambil data profile', e);
  }
}

function* profileSaga() {
  yield takeLatest('PROFILE', profileActionSaga);
}

export default profileSaga;
