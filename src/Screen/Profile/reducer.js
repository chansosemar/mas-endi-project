const initialState = {
  data: [],
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'PROFILE': {
      return {
        ...state,
      };
    }
    case 'PROFILE_SUCCESS': {
      return {
        ...state,
        data: action.payload,
      };
    }
    case 'UPDATED_PROFILE': {
      return {
        ...state,
      };
    }
    case 'UPDATED_PROFILE_SUCCESS': {
      return {
        ...state,
        data: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default profileReducer;
