import React, {useState} from 'react';
import {StyleSheet, Text, View, ScrollView, SafeAreaView} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import Head from '../../Component/Header';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Input from '../../Component/Input';
import Submit from '../../Component/Button';

import {updateProfileAction} from './action';
import {Avatar} from 'react-native-elements';

import {useDispatch, useSelector} from 'react-redux';

export default function editProfile(props) {
  const dispatch = useDispatch();
  const dataProfile = useSelector(state => state.Profile.data);

  const [email, setEmail] = useState('');
  const [Username, setUsername] = useState('');
  const [Password, setPassword] = useState('');
  const [fullName, setFullname] = useState('');

  const data = [
    {
      title: 'Fullname',
      data: fullName,
      setData: setFullname,
    },
    {
      title: 'Email',
      data: email,
      setData: setEmail,
    },
  ];

  const submitData = () => {
    const dataProfile = {
      full_name: fullName,
      email: email,
    };
    dispatch(updateProfileAction(dataProfile));
  };
  return (
    <SafeAreaView>
      <Head
        LeftPress={() => props.navigation.navigate('Profile')}
        leftIcon="close"
        title="Edit Profile"
        rightIcon="check"
        RightPress={submitData}
      />
      <ScrollView contentContainerStyle={styles.backgroundColor}>
        <View style={{paddingTop: moderateScale(50)}}>
          <Avatar
            size={120}
            rounded
            source={{
              uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
            }}>
            <Avatar.Accessory
              name="pencil-alt"
              type="font-awesome-5"
              size={22}
            />
          </Avatar>
        </View>
        <View style={styles.inputContainer}>
          <Input
            placeholder={dataProfile && dataProfile.full_name}
            onChangeText={text => setFullname(text)}
            value={data}
          />
          <Input
            placeholder="Username"
            onChangeText={text => setUsername(text)}
          />
          <Input
            placeholder={dataProfile && dataProfile.email}
            onChangeText={text => setEmail(text)}
            value={data}
            // editable={false}
          />
          <Input
            placeholder="Password"
            onChangeText={text => setPassword(text)}
            secureTextEntry
          />
        </View>
        <View style={styles.submit}>
          <Submit
            press={() => {
              dispatch({type: 'LOG OUT'});
              props.navigation.navigate('Login');
            }}
            title="LOG OUT"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  backgroundColor: {
    height: heightPercentageToDP(90),
    width: widthPercentageToDP(100),
    backgroundColor: 'black',
    alignItems: 'center',
  },
  profileBG: {
    backgroundColor: 'orange',
    width: moderateScale(100),
    height: moderateScale(100),
    top: heightPercentageToDP(8),
    borderRadius: moderateScale(100),
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: moderateScale(50),
  },
  inputContainer: {
    top: heightPercentageToDP(5),
    bottom: heightPercentageToDP(5),
    height: heightPercentageToDP(40),
    justifyContent: 'space-evenly',
  },
  submit: {
    marginTop: heightPercentageToDP(8),
  },
});
