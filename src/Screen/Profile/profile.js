import React, {useState, useEffect} from 'react';
import {Modal, StyleSheet, Text, View} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import Fontisto from 'react-native-vector-icons/Fontisto';
import {useDispatch, useSelector} from 'react-redux';
import Submit from '../../Component/Button';
import test from '../../Assets/Images/tes.png';
import {Avatar} from 'react-native-elements';

export default function profile(props) {
  const dispatch = useDispatch();
  const dataProfile = useSelector(state => state.Profile.data);
  const getProfile = () => {
    dispatch({type: 'PROFILE'});
  };
  console.log(dataProfile, 'test');

  useEffect(() => {
    getProfile();
  }, []);

  return (
    <View style={styles.backgroundColor}>
      <View>
        <Avatar
          size={150}
          rounded
          source={{
            uri: 'https://aesusdesign.com/wp-content/uploads/2019/06/mans-blank-profile-768x768.png',
          }}
          containerStyle={{paddingTop: moderateScale(15)}}>
          <Avatar.Accessory
            onPress={() => props.navigation.navigate('editProfile')}
            name="pencil-alt"
            type="font-awesome-5"
            size={22}
          />
        </Avatar>
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.Text}>{dataProfile && dataProfile.full_name}</Text>
        <Text style={styles.Text}>Username</Text>
        <Text style={styles.Text}>{dataProfile && dataProfile.email}</Text>
      </View>
      <Submit
        press={() => props.navigation.navigate('editProfile')}
        title="SETTINGS"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundColor: {
    padding: 20,
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
  },
  mainContainer: {
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(5),
    height: heightPercentageToDP(92),
    width: widthPercentageToDP(100),
    backgroundColor: 'black',
  },
  profileBG: {
    backgroundColor: 'orange',
    width: moderateScale(100),
    height: moderateScale(100),
    top: heightPercentageToDP(10),
    borderRadius: moderateScale(100),
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: moderateScale(50),
  },
  textContainer: {
    marginVertical: heightPercentageToDP(10),
    alignItems: 'center',
    justifyContent: 'space-around',
    height: heightPercentageToDP(25),
  },
  Text: {
    color: 'white',
    fontSize: 25,
  },
});
