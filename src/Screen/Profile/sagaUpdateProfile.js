import {takeLatest, put} from 'redux-saga/effects';
import {updateProfileApi} from './apiProfile';
import {updateProfileAction} from './action';
import {getHeaders} from '../utils/asynch';
import {navigate} from '../../Function/nav';
import {ToastAndroid} from 'react-native';

function* sagaUpdateProfile(action) {
  console.log(action, 'action');
  try {
    const headers = yield getHeaders();
    const data = {
      headers: headers,
      payload: action.payload,
    };
    const res = yield updateProfileApi(data);
    console.log(res, 'saga');
    if (res && res.data) {
      ToastAndroid.showWithGravity(
        'Update Berhasil',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      //   yield navigate('Profile');
      //   yield put(updateProfileActionSuccess(res.data.result));
    } else {
      ToastAndroid.showWithGravity(
        `Gagal update data`,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      yield put(loginActionFailed());
    }
  } catch (e) {
    ToastAndroid.showWithGravity(
      `${e}`,
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );
  }
}

function* updateProfileSaga() {
  yield takeLatest(updateProfileAction, sagaUpdateProfile);
}

export default updateProfileSaga;
