import axios from 'axios';

export const getProfileApi = payload => {
  return axios({
    method: 'GET',
    url: 'https://milantv-be.herokuapp.com/api/milantv/user/profile',
    headers: {Authorization: `${payload.Authorization}`},
    validateStatus: status => status < 500,
  });
};

export const updateProfileApi = data => {
  console.log(data, 'api');
  return axios({
    method: 'PUT',
    url: 'https://milantv-be.herokuapp.com/api/milantv/user/profile/update',
    headers: data.headers,
    data: {
      full_name: `${data.payload.full_name}`,
      email: `${data.payload.email}`,
    },
    validateStatus: status => status < 500,
  });
};
