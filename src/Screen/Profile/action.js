export const profileAction = payload => {
  return {type: 'PROFILE', payload};
};
export const profileActionSuccess = payload => {
  return {type: 'PROFILE_SUCCESS', payload};
};
export const updateProfileAction = payload => {
  return {type: 'UPDATED_PROFILE', payload: payload};
};
export const updateProfileActionSuccess = payload => {
  return {type: 'UPDATED_PROFILE_SUCCESS', payload};
};
