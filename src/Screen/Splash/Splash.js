import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {moderateScale} from 'react-native-size-matters';
import logo from '../../Assets/Images/image1.png';

export default function Splash(props) {
  useEffect(() => {
    setTimeout(() => {
      props.navigation.replace('Register');
    }, 2000);
  }, [props]);
  return (
    <View style={styles.container}>
      <FastImage source={logo} style={styles.logo} resizeMode="contain" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: moderateScale(140),
    height: moderateScale(120),
  },
});
