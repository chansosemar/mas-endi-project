export const registerAction = payload => {
  return {type: 'REGISTER', payload};
};
export const registerActionSuccess = payload => {
  return {type: 'REGISTER_SUCCESS', payload};
};
export const registerActionFailed = payload => {
  return {type: 'REGISTER_FAILED', payload};
};
export const resetAuthAction = payload => {
  return {type: 'RESET_AUTH'};
};
