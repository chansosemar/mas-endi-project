const initialState = {
  isRegister: false,
  isLogin: false,
  isLoading: false,
  data: [],
  dataRegister: [],
};

export default RegisterReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REGISTER': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'REGISTER_SUCCESS': {
      return {
        ...state,
        isRegister: true,
        isLoading: false,
        dataRegister: action.payload,
      };
    }
    case 'REGISTER_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'RESET': {
      return {
        ...state,
        isRegister: false,
        isLogin: false,
        isLoading: false,
        data: [],
      };
    }
    default:
      return state;
  }
};
