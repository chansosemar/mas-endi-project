export const setDataReview = data => {
  return {
    type: 'SET_REVIEW',
    payload: data,
  };
};
