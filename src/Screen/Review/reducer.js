const initialState = {
  headline: '',
  review: '',
  title: '',
  rating: '',
};

export const ReviewReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_REVIEW':
      return {
        headline: action.payload.headline,
        review: action.payload.review,
        title: action.payload.title,
        rating: action.payload.rating,
      };
    default:
      return state;
  }
};
