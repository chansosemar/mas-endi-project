const initialState = {
  isRegister: false,
  isLogin: false,
  isLoading: false,
  data: [],
  dataRegister: [],
};

export default loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        isLogin: true,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'LOGIN_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'LOGOUT': {
      return {
        ...state,
        isLogin: false,
      };
    }
    default:
      return state;
  }
};
