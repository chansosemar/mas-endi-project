export const loginAction = payload => {
  return {type: 'LOGIN', payload};
};
export const loginActionSuccess = payload => {
  return {type: 'LOGIN_SUCCESS', payload};
};
export const loginActionFailed = payload => {
  return {type: 'LOGIN_FAILED', payload};
};

export const logOut = payload => {
  return {type: 'LOGOUT', payload};
};
